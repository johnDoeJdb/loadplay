<?php
require_once 'vendor/autoload.php';

header('Content-Type: application/json');
$senderEmail = $_GET['sender'];
$status = false;
$msg = 'Некорректный формат эл. почты';
$from = 'loadplay.noreplay@gmail.com';
$to = 'to@loadplay.com';
$pass = 'qwe123ewq';

if (filter_var($senderEmail, FILTER_VALIDATE_EMAIL) !== false) {
	$message = Swift_Message::newInstance()
			->setSubject('Предзаказ на услуги Loadplay')
			->setFrom(array($from => 'Loadplay'))
			->setTo(array($to))
			->setBody('От пользователь с email: '.$senderEmail.'')
	;

	$transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, "ssl")
			->setUsername($from)
			->setPassword($pass)
			->setSourceIp('0.0.0.0')
	;

	$mailer = Swift_Mailer::newInstance($transport);
	$result = $mailer->send($message);
	if($result)
	{
		$status = true;
		$msg = "Сообщение отправлено";
	}
	else
	{
		$msg = "Ошибка во время отправки";
	}

}

echo "{\"status\" : \"{$status}\", \"message\": \"{$msg}\"}";

