#!/usr/bin/env sh
SRC_DIR="`pwd`"
cd "`dirname "$0"`"
cd "../tobie/ua-parser/php/bin"
BIN_TARGET="`pwd`/uaparser.php"
cd "$SRC_DIR"
"$BIN_TARGET" "$@"
