<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>LoadPlay</title>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Ubuntu:400,500,300&subset=cyrillic,latin">
    <link rel="stylesheet" href="./css/normalize.css">
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>
<?php
    require_once './vendor/autoload.php';
    use UAParser\Parser;
    $userAgent = $_SERVER['HTTP_USER_AGENT'];
    $parser = Parser::create();
    $result = $parser->parse($userAgent);
    if (strpos($result->os->family, 'mac') !== false) {
        $os_prefix = 'mac';
    } else {
        $os_prefix = 'win';
    }
?>
<article class="video">
    <header class="main-header">
        <div class="wrapper">
            <a href="/" class="main-header__logo">Loadplay</a>
            <nav class="main-header__menu">
                <a href="#watch" class="main-header__link link">Как это работает</a>
                <a href="#preOrder" class="main-header__btn btn">Сделать предзаказ</a>
            </nav>
        </div>
    </header>
    <video class="video__src" loop autoplay poster="./img/bg-video.jpg" id="video">
        <source src="/media/witcher.mp4" type="video/mp4">
    </video>
    <div class="intro wrapper">
        <h1>Играйте в любую игру на любом устройстве</h1>
        <p class="games__text">Забудьте о производительности своего компьютера. Играйте в облаке.</p>
        <a href="" class="intro__btn btn">Сделать предзаказ</a>
    </div>
</article>
<article class="games games--<?php echo $os_prefix ?>">
    <div class="wrapper">
        <h2>Играйте в любые игры</h2>

        <p>На любом компьютере, планшете и смартфоне</p>
    </div>
    <input id="tab1" name="tab" type="radio" checked>
    <input id="tab2" name="tab" type="radio">
    <input id="tab3" name="tab" type="radio">
    <input id="tab4" name="tab" type="radio">
    <input id="tab5" name="tab" type="radio">

    <div class="games__controls">
        <div class="wrapper">
            <div class="games__control">
                <label for="tab1" class="games__link games__link--gta link"><span>GTA V</span></label>
                <label for="tab2" class="games__link games__link--w3 link"><span>Ведьмак 3</span></label>
                <label for="tab3" class="games__link games__link--elite link"><span>Elite: Dangerous</span></label>
                <label for="tab4" class="games__link games__link--wow link"><span>World of Warcraft</span></label>
                <label for="tab5" class="games__link games__link--bf4 link"><span>Battlefield 4</span></label>
            </div>
            <p class="games__baloon">Любые игры
                из Steam, Origins,
                Battle.Net, и т. д.</p>
        </div>
    </div>
    <div class="games__tabs">
        <div class="wrapper">
            <div class="games__tab">Grand Theft Auto V</div>
            <div class="games__tab">Witcher 3</div>
            <div class="games__tab">Elite: Dangerous</div>
            <div class="games__tab">World of Warcraft</div>
            <div class="games__tab">Battlefield 4</div>
        </div>
    </div>
</article>
<article class="play play--<?php echo $os_prefix ?>">
    <div class="wrapper">
        <h2>Вы не почувствуете разницы</h2>
        <ul class="play__list">
            <li class="play__item play__item--launch">Вы запускаете наше приложение на любом компьютере, планшете или
                телефоне
            </li>
            <li class="play__item play__item--server">Устанавливаете и запускаете игру на удалённом рабочем столе у нас
                на сервере
            </li>
            <li class="play__item play__item--game">Теперь вы управляете игрой со своего компьютера, а мы в реальном
                времени показываем вам видео в высоком качестве 1080p, 60 FPS
            </li>
        </ul>
    </div>
</article>
<article class="watch wrapper" id="watch">
    <h2>Посмотрите сами</h2>

    <div class="watch__item">
        <video style="width: 404px; height=227px;" controls>
            <source src="/media/before.mp4" type="video/mp4">
        </video>
        <p>«Spintires» на слабом ноутбуке</p>
    </div>
    <div class="watch__item">
        <video style="width: 404px; height=227px;" controls>
            <source src="/media/after.mp4" type="video/mp4">
        </video>
        <p>«Spintires» на том же ноутбуке запущена по технологии loudPlay</p>
    </div>
</article>
<article class="partners">
    <div class="wrapper">
        <h2>Hаши партнёры</h2>
        <a class="partners__link partners__link--huawei" href="http://www.huawei.com/ru/">Huawei</a>
        <a class="partners__link partners__link--nvidia"
           href="http://www.nvidia.ru/object/nvidia-grid-ru.html">Nvidia</a>
        <a class="partners__link partners__link--dataline" href="http://www.dtln.ru/">Dataline</a>
    </div>
</article>
<article class="computer wrapper">
    <h2>Давайте разберёмся</h2>

    <p class="computer__text">Сколько стоит хороший игровой компьютер</p>
    <ul class="part">
        <li class="part__item part__item--system">
            <p class="part__name">Сист. блок</p>

            <p class="part__price">8 000 <span class="icon-rouble">Р</span></p>
        </li>
        <li class="part__item part__item--monitor">
            <p class="part__name">Монитор</p>

            <p class="part__price">15 000 <span class="icon-rouble">Р</span></p>
        </li>
        <li class="part__item part__item--mainboard">
            <p class="part__name">Мат. плата</p>

            <p class="part__price">15 000 <span class="icon-rouble">Р</span></p>
        </li>
        <li class="part__item part__item--videocard">
            <p class="part__name">Видеокарта</p>

            <p class="part__price">25 000 <span class="icon-rouble">Р</span></p>
        </li>
        <li class="part__item part__item--ram">
            <p class="part__name">Опер. память</p>

            <p class="part__price">9 000 <span class="icon-rouble">Р</span></p>

            <p class="part__memory">за 16 Гб</p>
        </li>
        <li class="part__item part__item--processor">
            <p class="part__name">Процессор</p>

            <p class="part__price">30 000 <span class="icon-rouble">Р</span></p>
        </li>
        <li class="part__item part__item--drive">
            <p class="part__name">SSD-диск</p>

            <p class="part__price">9 000 <span class="icon-rouble">Р</span></p>

            <p class="part__memory">за 240 Гб</p>
        </li>
        <li class="part__item part__item--supply">
            <p class="part__name">Блок питания</p>

            <p class="part__price">6 000 <span class="icon-rouble">Р</span></p>
        </li>

        <li class="part__item part__item--cooler">
            <p class="part__name">Кулер</p>

            <p class="part__price">3 000 <span class="icon-rouble">Р</span></p>
        </li>
    </ul>
    <div class="computer__total">
        <h2>Итого 120 000 руб.</h2>

        <p>Или <span>3 300 рублей</span> в месяц в течение трёх лет</p>
    </div>
</article>
<article class="buy" id="preOrder">
    <div class="wrapper">
        <h2>Мы запустимся в октябре</h2>

        <p>Оформите предзаказ до 15 сентября со скидкой 50%</p>

        <form action="" class="buy__form">
            <p class="buy__price"><span class="buy__old">1590 <span class="icon-rouble">Р</span></span> <span>790 <span
                        class="icon-rouble">Р</span></span> за месяц</p>

            <p class="buy__text">Для первых ста заказавших цена останется такой навсегда</p>
            <input type="email" name="email" id="email" placeholder="Эл. почта">
            <button type="submit" class="buy__btn btn" disabled name="order" id="order">Заказать</button>
        </form>
    </div>
</article>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js"></script>
<script>
    $(function () {
        $.validator.setDefaults({
            success: "false"
        });
        $(".buy__form").validate({
            showErrors: function (errorMap, errorList) {
            },
            rules: {
                email: {
                    required: true,
                    email: true
                }
            }
        });
        $('#email').on('keyup change focusout', function () {
            if ($(".buy__form").valid() && $('button#order').attr("disabled")) {
                $('button#order').removeAttr('disabled');
            } else if (!$(".buy__form").valid() && !$('button#order').attr("disabled")) {
                $('button#order').attr('disabled', 'disabled');
            }
        });
        $('button#order').on('click', function (e) {
            e.preventDefault();
            var email = $('#email').val();
            $.ajax({
                url: '/sendEmail.php',
                data: {sender: email},
                success: function () {
                    alert('Ваш email успешно отправлен и в скором времени будет обработан нашими сотрудниками.');
                    $('#email').val('');
                }
            })
        });
        $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });
</script>
</body>
</html>